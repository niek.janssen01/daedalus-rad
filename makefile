do: createstartscript

createstartscript: copyclient buildserver
	echo "cd rad-server && node dist/index.js" > start.sh
	chmod +x start.sh

copyclient: buildclient
	rm -rf rad-server/rad-client
	cp -r rad-client/dist/rad-client rad-server/rad-client

buildclient: installclient installglobal
	cd rad-client && ng build --prod

installclient: 
	cd rad-client && npm i

buildserver: installserver installglobal
	cd rad-server && tsc

installserver:
	cd rad-server && npm i

installglobal:
	sudo npm -g i @angular/cli typescript

