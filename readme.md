# Daedalus Rad

Want bierlijsten maken zichzelf niet

## Setup
### Linux:

  - Zorg dat make, nodejs en npm op je systeem staan
  - Cd in de repository en voor `make` uit
  - Pas eventueel de configuratie in `rad-server/config.json5` aan. 
  - Gebruik het startscript om het systeem te starten
  - Ga naar `localhost:8080` om dingen te kunnen zien

### Windows:

  - Zorg de stappen van Linux, maar voer de stappen uit de makefile zelf uit. 

### Mac:

  - Ja geen idee, doe maar hetzelfde als windows ofzo (of linux als make werkt,
      idk)

## De applicatie

  - Random borrelpersoon:
    - Klik op een naam
    - Voeg dingen toe aan jouw persoonlijke queue
  - Beamer / tv:
    - Klik op de tekst "de bierlijst"
    - Draai aan het rad
    - Klik op het nummer dat je gedraait hebt
