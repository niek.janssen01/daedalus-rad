import {DragDropModule}   from '@angular/cdk/drag-drop';
import {HttpClientModule} from '@angular/common/http';
import {NgModule}         from '@angular/core';
import {FormsModule}      from '@angular/forms';
import {BrowserModule}    from '@angular/platform-browser';
import {BeamerComponent}  from './components/beamer/beamer';
import {QueueComponent}   from './components/queue/queue';

import {RootComponent}     from './components/root/root';
import {UserlistComponent} from './components/userlist/userlist';

@NgModule({
              declarations: [RootComponent,
                             UserlistComponent,
                             QueueComponent,
                             BeamerComponent],
              imports     : [BrowserModule,
                             HttpClientModule,
                             DragDropModule,
                             FormsModule],
              providers   : [],
              bootstrap   : [RootComponent],
          })
export class AppModule {
}
