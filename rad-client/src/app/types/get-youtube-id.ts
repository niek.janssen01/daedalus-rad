declare module 'get-youtube-id' {
    function yid (url: string): string;

    export = yid;
}
