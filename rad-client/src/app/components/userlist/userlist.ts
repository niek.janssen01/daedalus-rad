import {Component}   from '@angular/core';
import {UserService} from '../../services/user.service';

@Component({
               selector   : 'app-userlist',
               templateUrl: './userlist.html',
               styleUrls  : ['./userlist.sass'],
           })
export class UserlistComponent {
    constructor (public userservice: UserService) {
    }
}
