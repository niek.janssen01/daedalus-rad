import {Component, HostListener} from '@angular/core';
import {UserService}             from '../../services/user.service';

@Component({
               selector   : 'app-root',
               templateUrl: './root.html',
               styleUrls  : ['./root.sass'],
           })
export class RootComponent {
    constructor (public userService: UserService) {
    }

    @HostListener('window:keyup', ['$event'])
    handleKonamy (event: KeyboardEvent) {
        this.userService.keypress(event.key);
    }
}
