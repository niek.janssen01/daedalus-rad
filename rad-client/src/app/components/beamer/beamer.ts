import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {DomSanitizer, SafeResourceUrl}            from '@angular/platform-browser';
import {WheelService}                             from '../../services/wheelservice';

@Component({
               selector   : 'app-beamer',
               templateUrl: './beamer.html',
               styleUrls  : ['./beamer.sass'],
           })
export class BeamerComponent implements OnInit {
    @ViewChild('backgroundyt') backgroundYt: ElementRef | undefined;
    width     = 100;
    height    = 100;
    cachedUrl = '';
    sanitizedUrl: SafeResourceUrl;

    constructor (public wheelService: WheelService, private sanitizer: DomSanitizer) {
        this.sanitizedUrl = sanitizer.bypassSecurityTrustResourceUrl('');
    }

    url () {
        let url = '';
        if (this.wheelService.current !== undefined)
            url = 'https://www.youtube.com/embed/' + this.wheelService.current.id + '?&autoplay=1';

        if (url === this.cachedUrl)
            return this.sanitizedUrl;

        this.sanitizedUrl = this.sanitizer.bypassSecurityTrustResourceUrl(url);
        this.cachedUrl    = url;
        return this.sanitizedUrl;
    }

    ngOnInit (): void {
        this.resize();
    }

    resize () {
        if (this.backgroundYt === undefined)
            return;
        this.width  = this.backgroundYt.nativeElement.offsetWidth;
        this.height = this.backgroundYt.nativeElement.offsetHeight;
    }
}
