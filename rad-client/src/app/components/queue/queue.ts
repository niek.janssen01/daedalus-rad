import {CdkDragDrop, moveItemInArray} from '@angular/cdk/drag-drop';
import {Component}                    from '@angular/core';
import {QueueService}                 from '../../services/queue.service';

@Component({
               selector   : 'app-queue',
               templateUrl: './queue.html',
               styleUrls  : ['./queue.sass'],
           })
export class QueueComponent {
    url = '';

    constructor (public queueService: QueueService) {
    }

    reorder (event: CdkDragDrop<string[]>) {
        moveItemInArray(this.queueService.queue, event.previousIndex, event.currentIndex);
        this.queueService.post();
    }

    enqueue () {
        this.queueService.enqueue(this.url);
        this.url = '';
    }
}
