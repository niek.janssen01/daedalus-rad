export interface QueueItem {
    id: string;
    name: string;
    thumb: string;
    user: string;
    duration: number;
}

