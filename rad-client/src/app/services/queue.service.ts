import {HttpClient}      from '@angular/common/http';
import {Injectable}      from '@angular/core';
// @ts-ignore
import * as yid          from 'get-youtube-id';
import {duration}        from 'moment';
import {apikey, baseurl} from '../config';
import {QueueItem}       from '../models/queryitem';
import {UserService}     from './user.service';
import {WheelService}    from './wheelservice';

@Injectable({providedIn: 'root'})
export class QueueService {
    queue: QueueItem[] = [];
    lock               = true;
    alert              = false;

    constructor (private http: HttpClient, private user: UserService, private wheelService: WheelService) {
        this.getQueue();
    }

    post () {
        this.http.post(baseurl + 'queue/' + this.user.loginName, this.queue)
            .subscribe(() => {
                console.log(this.queue);
                this.wheelService.getwheel();
            });
    }

    remove (i: number) {
        this.queue.splice(i, 1);
        this.post();
    }

    async enqueue (url: string) {
        this.lock = true;
        const id  = yid(url);
        this.http.get(
            `https://www.googleapis.com/youtube/v3/videos?part=snippet%2C%20contentDetails&id=${id}&key=${apikey}`)
            .subscribe((data: any) => {
                try {
                    this.queue.push({
                                        id      : id,
                                        name    : data.items[0].snippet.title,
                                        thumb   : data.items[0].snippet.thumbnails.default.url,
                                        user    : this.user.loginName,
                                        duration: duration(data.items[0].contentDetails.duration)
                                            .asMilliseconds(),
                                    });
                    this.alert = false;
                    this.post();
                } catch {
                    this.alert = true;
                }
                this.lock = false;
            });
    }

    getQueue () {
        this.http.get(baseurl + 'queue/' + this.user.loginName)
            .subscribe((data: any) => {
                this.queue = data;
                this.lock  = false;
            });
    }
}
