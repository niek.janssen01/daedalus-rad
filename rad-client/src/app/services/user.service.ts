import {HttpClient}   from '@angular/common/http';
import {Injectable}   from '@angular/core';
import {baseurl}      from '../config';
import {WheelService} from './wheelservice';

@Injectable({providedIn: 'root'})
export class UserService {
    loginName          = '';
    userlist: string[] = [];
    beamer             = false;
    codePos            = 0;
    code               = ['ArrowUp', 'ArrowUp', 'ArrowDown', 'ArrowDown',
                          'ArrowLeft', 'ArrowRight', 'ArrowLeft', 'ArrowRight',
                          'b', 'a'];

    constructor (private http: HttpClient, private wheel: WheelService) {
        this.getUserlist();
    }

    keypress (key: string) {
        if (this.beamer)
            this.handleChooseKey(key);
        else
            this.handleKonamy(key);
    }

    private getUserlist () {
        this.http.get(baseurl + 'userlist')
            .subscribe((data: string[]) => this.userlist = data);
    }

    private handleKonamy (key: string) {
        if (this.code[this.codePos] === key)
            this.codePos++;
        else
            this.codePos = 0;
        if (this.codePos === this.code.length)
            this.beamer = true;
    }

    private handleChooseKey (key: string) {
        const n = parseInt(key, 36);
        if (n !== NaN)
            this.wheel.choose(n);
    }
}
