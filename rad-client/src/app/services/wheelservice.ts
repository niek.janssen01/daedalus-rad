import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {baseurl}    from '../config';
import {QueueItem}  from '../models/queryitem';

const emptyQueueItem: QueueItem = {
    duration: 1000, id: '', name: '', thumb: '', user: '',
};

@Injectable({providedIn: 'root'})
export class WheelService {
    wheel: QueueItem[] = [];
    current: QueueItem | undefined;
    next: QueueItem | undefined;

    constructor (public http: HttpClient) {
        this.getwheel();
    }

    choose (i: number) {
        if (this.next !== undefined)
            return;
        this.next = emptyQueueItem;
        this.http.post(baseurl + 'wheelturn', {nspinned: i})
            .subscribe((data: QueueItem) => {
                this.next = data;
                this.rotate(false);
                setTimeout(() => this.getwheel(), this.next === undefined ? 0 : 1100);
            });
    }

    rotate (force: boolean) {
        let to = false;
        if (force || this.current === undefined)
            to = true;

        if (force)
            this.current = undefined;
        if (this.current === undefined) {
            this.current = this.next;
            this.next    = undefined;
        }

        if (this.current !== undefined && to)
            setTimeout(() => this.rotate(true), this.current.duration);
    }

    getwheel () {
        this.http.get(baseurl + 'wheel')
            .subscribe((data: any) => this.wheel = data);
    }

    ban (event: MouseEvent, i: number) {
        event.preventDefault();
        this.http.post(baseurl + 'ban', {index: i})
            .subscribe((data: QueueItem) => {
                this.wheel[i] = data;
            });
    }
}
