export function sum (arr: number[]): number {
    return arr.reduce((a: number, b: number) => a + b, 0);
}
