export class CreatorWeakMap<K extends object, V> implements WeakMap<K, V> {
    map: WeakMap<K, V> = new WeakMap();
    readonly [Symbol.toStringTag]: 'WeakMap';

    constructor(private valueConstructor: (key: K) => V) {
    }

    clear() {
        this.map = new WeakMap();
    }

    delete(key: K): boolean {
        return this.map.delete(key);
    }

    get(key: K): V | undefined {
        return this.map.get(key);
    }

    has(key: K): boolean {
        return this.map.has(key);
    }

    set(key: K, value: V): this {
        this.map.set(key, value);
        return this;
    }

    getDef(key: K): V {
        if (!this.map.has(key))
            this.map.set(key, this.valueConstructor(key));
        return <V>this.map.get(key);
    }
}

export class CreatorMap<K, V> implements Map<K, V> {
    map: Map<K, V> = new Map();
    readonly [Symbol.toStringTag]: 'Map';

    constructor(private valueConstructor: (key: K) => V) {
    }

    get size(): number {
        return this.map.size;
    }

    clear() {
        this.map = new Map();
    }

    delete(key: K): boolean {
        return this.map.delete(key);
    }

    get(key: K): V | undefined {
        return this.map.get(key);
    }

    has(key: K): boolean {
        return this.map.has(key);
    }

    set(key: K, value: V): this {
        this.map.set(key, value);
        return this;
    }

    getDef(key: K): V {
        if (!this.map.has(key))
            this.map.set(key, this.valueConstructor(key));
        return <V>this.map.get(key);
    }

    [Symbol.iterator](): IterableIterator<[K, V]> {
        return this.map[Symbol.iterator]();
    }

    entries(): IterableIterator<[K, V]> {
        return this.map.entries();
    }

    forEach(callbackfn: (value: V, key: K, map: Map<K, V>) => void, thisArg?: any): void {
        this.map.forEach(callbackfn, thisArg);
    }

    keys(): IterableIterator<K> {
        return this.map.keys();
    }

    values(): IterableIterator<V> {
        return this.map.values();
    }
}
