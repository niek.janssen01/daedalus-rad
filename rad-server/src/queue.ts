import {promises as fs} from 'fs';
import {EOL}            from 'os';
import {config}         from './config';
import * as math        from './math';

interface QueueItem {
    id: string;
    name: string;
    thumb: string;
    user: string;
    duration: number;
}

export const queues: { [key: string]: QueueItem[] } = {};
const itemsQueued: { [key: string]: number }        = {};
const history: QueueItem[]                          = [];
export let wheel: QueueItem[]                       = [];
let banned: string[]                                = [];

for (const name of config.userlist) {
    queues[name]      = [];
    itemsQueued[name] = 0;
}

function asNum (arr: [string, number][]): number[] {
    return arr.map(([_, n]: [string, number]) => n);
}

function asNumFn (fn: (n: number) => number): ([s, n]: [string, number]) => [string, number] {
    return ([s, n]: [string, number]) => [s, fn(n)];
}

function computeEntries (): [string, number][] {
    const entries = config.userlist.map((name: string) => [name, itemsQueued[name]] as [string, number])
                          .filter(([name, _]: [string, number]) => queues[name].length > 0);
    const min     = Math.min(...asNum(entries));
    const reduced = entries.map(asNumFn((n: number) => 1 / (n - min + 1)));
    const sum     = math.sum(asNum(entries));
    return reduced.map(asNumFn((n: number) => n / sum));
}

function weighedChance (items: [string, number][]): string {
    const n         = Math.random();
    let accumulator = 0;
    for (const [name, value] of items) {
        accumulator += value;
        if (accumulator > n)
            return name;
    }
    return items[0][0];
}

export function genwheel (): QueueItem[] {
    const counters: { [key: string]: number } = {};
    for (const name of config.userlist)
        counters[name] = 0;

    const entries = computeEntries();
    if (entries.length === 0)
        return [];

    wheel = [];

    for (let i = 0; i < config.radsize; i++) {
        const name = weighedChance(entries);
        const item = queues[name][counters[name] % queues[name].length];
        wheel.push(item);
        counters[name]++;
    }
    return wheel;
}

export function wheelturn (n: number): QueueItem {
    const item = wheel[n];
    history.push(item);
    itemsQueued[item.user]++;

    const itemIndex = queues[item.user].findIndex((x: QueueItem) => x.id === item.id);
    queues[item.user].splice(itemIndex, 1);

    saveChoosing(item);

    genwheel();
    return item;
}

async function saveChoosing (item: QueueItem) {
    const file = await fs.open(config.history, 'a');
    if (file === null)
        return console.log('Cannot write history!!!!');
    await file.write('https://youtube.com/watch?v=' + item.id + EOL, null, 'utf-8');
    await file.close();
}

export function ban (index: number): QueueItem {
    banned.push(wheel[index].id);
    sanitizeAll();
    const entries = computeEntries();
    const name    = weighedChance(entries);
    const item    = queues[name][0];
    wheel[index]  = item;
    return item;
}

export function sanitize (queue: QueueItem[]): QueueItem[] {
    return queue
        .filter((item: QueueItem) => queue.findIndex((value: QueueItem) => item.id === value.id))
        .filter((item: QueueItem) => !(item.id in banned))
        .filter((item: QueueItem) => item.duration < (5 * 30 + 10) * 1000);
}

function sanitizeAll () {
    for (const name of config.userlist)
        queues[name] = sanitize(queues[name]);
}
