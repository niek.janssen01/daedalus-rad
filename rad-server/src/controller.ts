import {config}                                            from './config';
import {ban, genwheel, queues, sanitize, wheel, wheelturn} from './queue';

export function userlisthandler (req: any, res: any) {
    res.send(JSON.stringify(config.userlist));
}

export function postqueuehandler (req: any, res: any) {
    const name   = req.params.name;
    const queue  = req.body;
    queues[name] = sanitize(queue);
    if (wheel.length === 0)
        genwheel();
    getqueuehandler(req, res);
}

export function getqueuehandler (req: any, res: any) {
    const name = req.params.name;
    res.send(JSON.stringify(queues[name]));
}

export function wheelhandler (req: any, res: any) {
    res.send(JSON.stringify(wheel));
}

export function wheelturnhandler (req: any, res: any) {
    res.send(JSON.stringify(wheelturn(req.body.nspinned)));
}

export function banhandler (req: any, res: any) {
    res.send(JSON.stringify(ban(req.body.index)));
}
