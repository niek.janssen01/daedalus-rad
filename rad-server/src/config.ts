import {readFileSync} from 'fs';

const json5 = require('json5');

interface Config {
    userlist: string[]
    radsize: number
    history: string
}

function getConfig (): Config {
    const json = readFileSync('config.json5', 'utf-8');
    return json5.parse(json);
}

export const config = getConfig();
