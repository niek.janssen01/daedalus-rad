import {filenames, statichandler} from './app';
import {
    banhandler,
    getqueuehandler,
    postqueuehandler,
    userlisthandler,
    wheelhandler,
    wheelturnhandler,
}                                 from './controller';

const express    = require('express');
const json5      = require('json5');
const cors       = require('cors');
const bodyparser = require('body-parser');
const morgan     = require('morgan');

const app = express();
app.use(cors());
app.use(bodyparser.json());
app.use(morgan('tiny'));

// Static routes
for (const filename of filenames)
    app.get(`/${filename}`, statichandler(filename));
app.get('/', statichandler('index.html'));

// API routes
app.get('/userlist', userlisthandler);
app.post('/queue/:name', postqueuehandler);
app.get('/queue/:name', getqueuehandler);
app.get('/wheel', wheelhandler);
app.post('/wheelturn', wheelturnhandler);
app.post('/ban', banhandler);

app.listen(8080);
