import {readdirSync, readFileSync} from 'fs';

const files: { [key: string]: string } = {};
export const filenames                 = readdirSync('rad-client');

for (const filename of filenames) {
    files[filename] = readFileSync(`rad-client/${filename}`, 'utf-8');
}

export function statichandler (filename: string): (req: any, res: any) => void {
    return (req: any, res: any) => {
        res.contentType(filename);
        res.send(files[filename]);
    };
}
